SetOptions[$Output,PageWidth->Infinity];
Get[DirectoryName[$InputFileName] <> "/../MBcreate.m"];
data = Get[DirectoryName[$InputFileName] <> "/MBcreateData.m"];
For[i=1, i<=Length[data], ++i,
    Print[i];
    res = MBcreate[data[[i,1]], Verbose->False];
    vars = Variables[res/.Gamma->List];
    l = Max @@ List @@@ Append[Cases[vars, z[_]], z[0]];
    If[l === data[[i,2]],
        Print["Test passed"];
    ,
        Print["Test failed: expected ", data[[i,2]], " got ", l];
    ]
];
