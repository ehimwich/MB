(******************************************************************************
 *                                                                            *
 * Asymptotic expansions of Mellin-Barnes integrals.                          *
 *                                                                            *
 * M. Czakon, 12 Dec 06                                                       *
 *                                                                            *
 ******************************************************************************)

MBasymptotics::usage = "MBasymptotics[integrals, {var, order}] expands a list
of integrals in variable var, around var = 0, up to the given order by closing
contours and taking residues. The integrands may contain polynomials in var,
but denominators which aren't products of Gamma functions are forbidden."

MBasymptotics::exp = "more than one exponent with integration variables";

MBasymptotics[integrals_List, {var_, order_}] :=
Module[{ints = Cases[integrals, MBint[__], -1], int, rul, vars, exponents, bound,
  integrand, pow, x, y, n, m, i},

  (* simplification of powers *)

  ints = ExpandAll[ints, var] //.
    {(x_*y_)^n_ -> x^n*y^n, (x_^n_)^m_ :> x^Expand[n*m]};
  ints = ints /. var^n_. -> pow[var, n];
  ints = ints /. MBint[int_, rul_] :> Collect[int, pow[__], MBint[#, rul]&];
  ints = ints /. pow[var, n_]*MBint[int_, rul_] :> MBint[var^n*int, rul];
  ints = ints /. pow[var, n_] -> var^n;
  ints = If[Head[#] === Plus, List @@ #, #]& /@ ints;
  ints = Flatten[ints];

  (* expansion by taking residues *)

  ints = Flatten[ints //. MBint[int_, rul_] :>
    MBasymptotics[MBint[int, rul], {var, order}]];

  Return[ints]]

MBasymptotics[MBint[integrand_, rules_], {var_, order_}] :=
Module[{vars = Alternatives @@ (#[[1]]& /@ rules[[2]]), subst = Flatten[rules],
  exponents, exp, sign, z, z0, zz, args, poles, v, v0, v1, i, contribs,
  stripped},

  If[FreeQ[integrand, var],
    If[order < 0, Return[{}], Return[MBint[integrand, rules]]]];

  exponents = First[Exponent[{integrand /. Log[var]->lvar}, var, List]];
  If[Length[exponents] > 1, Message[MBasymptotics::exp]; Throw[{}]];

  exp = First[exponents];
  If[order < exp /. subst, Return[{}]];
  If[FreeQ[exp, vars], Return[MBint[integrand, rules]]];

  (* the choice of the variable in which residues will be taken, this might be
     improved in the future *)

  z = First[Cases[{exp}, vars, -1]];
  z0 = Solve[order == exp /. z -> zz /. subst, zz][[1, 1, 2]];

  sign = Sign[Coefficient[exp, z]];

  args = Union[Cases[{Numerator[integrand]},
    (Gamma | PolyGamma)[___, zz_] :> zz /; MemberQ[{zz}, z, -1], -1]];

  poles = Union[Flatten[Table[
    v0 = args[[i]] /. subst;
    v1 = args[[i]] /. z -> z0 /. subst;
    If[v0 > v1, {v0, v1} = {v1, v0}];
    v0 = Min[Ceiling[v0], 1];
    v1 = Min[Floor[v1], 0];
    Table[Solve[args[[i]] == v, z][[1]], {v, v0, v1}],
  {i, Length[args]}]]];

  stripped = DeleteCases[rules, z -> _, -1];

  contribs = Table[
    MBint[ExpandAll[-sign*MBresidue[integrand, List @@ poles[[i]]]], stripped],
  {i, Length[poles]}];

  Return[contribs]]
